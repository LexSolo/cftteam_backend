package CFTTeamDemo.EmployerDemo.repos;

import CFTTeamDemo.EmployerDemo.domain.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Возможно это для БД уже нужно.
 * */

public interface EmployerRepo extends JpaRepository<Employer, Long> {
}
