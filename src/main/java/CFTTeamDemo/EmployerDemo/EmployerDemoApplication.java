package CFTTeamDemo.EmployerDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployerDemoApplication.class, args);
	}

}
