package CFTTeamDemo.EmployerDemo.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

/**
 * Сущность.
 * */

@Entity
//@Table
@ToString(of = {"id", "name"})
@EqualsAndHashCode(of = {"id"})
public class Employer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
