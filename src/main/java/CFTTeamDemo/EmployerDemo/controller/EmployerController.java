package CFTTeamDemo.EmployerDemo.controller;

import CFTTeamDemo.EmployerDemo.exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Контроллер для REST с методами.
 * */

@RestController
@RequestMapping("employer")
public class EmployerController {
    private int counter = 4;

    private List<Map<String, String>> employers = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String, String>() {{ put("id", "1"); put("name", "First employer"); }});
        add(new HashMap<String, String>() {{ put("id", "2"); put("name", "Second employer"); }});
        add(new HashMap<String, String>() {{ put("id", "3"); put("name", "Third employer"); }});
    }};

    @GetMapping
    public List<Map<String, String>> list() {
        return employers;
    }

    @GetMapping("{id}")
    public Map<String, String> getOne(@PathVariable String id) {
        return getEmployer(id);
    }

    private Map<String, String> getEmployer(@PathVariable String id) {
        return employers.stream()
                .filter(employer -> employer.get("id").equals(id))
                .findFirst().orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Map<String, String> create(@RequestBody Map<String, String> employer) {
        employer.put("id", String.valueOf(counter++));

        employers.add(employer);

        return employer;
    }

    @PutMapping("{id}")
    public Map<String, String> update(@PathVariable String id, @RequestBody Map<String, String> employer) {
        Map<String, String> employerFromDB = getEmployer(id);

        employerFromDB.putAll(employer);
        employerFromDB.put("id", id);

        return employerFromDB;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        Map<String, String> employer = getEmployer(id);

        employers.remove(employer);
    }
}
